# functionHelp #

I got fed up trying to document my R functions. Python has a very good easy way of documenting user defined functions but R sadly seem to not be that easy to do so. Unlike Python R has a more structured way of documenting functions (see roxygen). I have written a small function of users to include their own documentation within the function in R similar to Python. It may not be feature rich but nonetheless very useful.


```
library(functionHelp)

#### Your function definition
# The description gets between
# fhelp()
# " 
#  .... 
# "
# fhelp()

Yourfunction <- function(temp){

    fhelp()
    "
    Your description of the package
    "
    fhelp()
    package commands ...
}

#### Get help for your function
functionHelp("Yourfunction")
```