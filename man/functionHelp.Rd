\name{functionHelp}
\alias{functionHelp}
\title{functionHelp}
\description{
\packageDescription{functionHelp}
}
\details{

The DESCRIPTION file:
\packageDESCRIPTION{functionHelp}
\packageIndices{functionHelp}

\examples{
functionHelp("functionName")
}
